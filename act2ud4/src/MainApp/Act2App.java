/**
 * 
 */
package MainApp;

/**
 * @author Elisabet Sabat�
 *
 */
public class Act2App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		int N = 5;
		double A = 4.56;
		char C = 'a';
		
		System.out.println("N: "+N+" A: "+A+" C: "+C);
		
		System.out.println("N+A = "+(N+A) );
		System.out.println("N-A = "+(N-A) );
		System.out.println("Valor num�rico del car�cter "+C+" = "+(int)C);

	}

}
